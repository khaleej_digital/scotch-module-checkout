define([
    'jquery',
    'underscore',
    'uiComponent',
    'ko',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/quote',
    'mage/translate',
    'uiRegistry',
    'checkoutData'
], function (
    $,
    _,
    Component,
    ko,
    stepNavigator,
    quote,
    $t,
    uiRegistry,
    checkoutData
) {
    'use strict';

    return Component.extend({
        isVisible: ko.observable(true),
        qouteObject: ko.observable(null),
        shippingAddress: quote.shippingAddress,
        billingAddress: quote.billingAddress,
        paymentMethod: quote.paymentMethod,
        shippingMehtod: quote.shippingMethod,

        /** @inheritdoc */
        initialize: function () {
            this._super();
            stepNavigator.registerStep(
                'order-review-step',
                null,
                $t('Order Review'),
                this.isVisible,
                _.bind(this.navigate, this),
                this.sortOrder
            );

            return this;
        },

        navigate: function () {
            let self = this;
            self.isVisible(false);

            if (!self.hasShippingMethod()) {
                self.stepNavigator.setHash('shipping');
            } else if (!self.hasPaymentMethod()) {
                self.stepNavigator.setHash('payment');
            } else {
                self.isVisible(true);
            }
        },

        hasShippingMethod: function () {
            return window.checkoutConfig.selectedShippingMethod !== null;
        },

        hasPaymentMethod: function () {
            return this.checkoutData.getSelectedPaymentMethod() !== null
                || typeof this.checkoutData.getSelectedPaymentMethod() !== 'undefined';
        },

        placeOrder: function () {
            $(".payment-method._active").find('.action-original').trigger('click');
        }
    });
});

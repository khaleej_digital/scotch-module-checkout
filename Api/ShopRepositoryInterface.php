<?php

namespace Beside\Checkout\Api;

interface ShopRepositoryInterface
{

    /**
     * @api
     * @param \Beside\Checkout\Api\Search\SearchCriteriaInterface $searchCriteria
     * @return \Redbox\Shopfinder\Api\Data\ShopSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Beside\Checkout\Api\Search\SearchCriteriaInterface $searchCriteria
    );

    /**
     * @param int $shopId
     *
     * @return \Redbox\Shopfinder\Api\Data\ShopInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($shopId);

    /**
     * @api
     * @return string[].
     */
    public function getAllStoresForBothLanguages();
}

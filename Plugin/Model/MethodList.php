<?php
namespace Beside\Checkout\Plugin\Model;

use Magento\OfflinePayments\Model\Cashondelivery;

class MethodList
{
    /**
     * @param \Magento\Payment\Model\MethodList $subject
     * @param $availableMethods
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return mixed
     */
    public function afterGetAvailableMethods(
        \Magento\Payment\Model\MethodList $subject,
        $availableMethods,
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
        $shippingMethod = $this->getShippingMethodFromQuote($quote);
        foreach ($availableMethods as $key => $method) {
            if($method->getCode() === Cashondelivery::PAYMENT_METHOD_CASHONDELIVERY_CODE
                && $shippingMethod === 'beside_store_pickup_beside_store_pickup')
            {
                unset($availableMethods[$key]);
            }
        }

        return $availableMethods;
    }

    /**
     * @param \Magento\Quote\Api\Data\CartInterface $quote
     * @return string
     */
    private function getShippingMethodFromQuote($quote)
    {
        if ($quote) {
            return $quote->getShippingAddress()->getShippingMethod();
        }

        return '';
    }
}

define([
    'ko',
    'jquery',
    'Magento_Customer/js/model/customer'
], function (ko, $, customerModel) {

    let customerData = {
        'firstname' : null,
        'lastname' : null,
        'email' : null,
        'telephone' : null,
        'street' : null,
        'streetopt' : null
    };

    if (customerModel.isLoggedIn()) {
        if (typeof customerModel.customerData !== 'undefined') {
            if (typeof customerModel.customerData.firstname !== 'undefined') {
                customerData.firstname = customerModel.customerData.firstname;
            }

            if (typeof customerModel.customerData.lastname !== 'undefined') {
                customerData.lastname = customerModel.customerData.lastname;
            }

            if (typeof customerModel.customerData.email !== 'undefined') {
                customerData.email = customerModel.customerData.email;
            }
        }

        $.each(customerModel.getShippingAddressList(), function(index, value) {
            if (value.isDefaultShipping()) {
                if (typeof value.street[0] !== 'undefined') {
                    customerData.street = value.street[0];
                }

                if (typeof value.street[1] !== 'undefined') {
                    customerData.streetopt = value.street[1];
                }

                if (typeof value.telephone !== 'undefined') {
                    customerData.telephone = value.telephone;
                }
            }
        });
    }

    return {
        firstname: ko.observable(customerData.firstname),
        lastname: ko.observable(customerData.lastname),
        email: ko.observable(customerData.email),
        telephone: ko.observable(customerData.telephone),
        street: ko.observable(customerData.street),
        streetopt: ko.observable(customerData.streetopt),
        selectedShop: ko.observable(null),
        locations: ko.observableArray([])
    }
});

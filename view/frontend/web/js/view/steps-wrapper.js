define([
    'jquery',
    'ko',
    'uiComponent',
    'accordion',
    'loader'
], function ($, ko, Component) {
    'use strict';

    return Component.extend({
        shippingStepActive: ko.observable(true),
        paymentStepActive: ko.observable(false),
        orderReviewStepActive: ko.observable(false),
        shippingStepProcessed: ko.observable(false),
        accardionId: '#one-step-checkout-accordion',
        shippingStepIndex: 0,
        paymentStepIndex: 1,
        orderReviewStepIndex: 2,
        mapStepIndexToId: ['#shipping-step-tab', '#payemnt-step', '#order-review-step-tab'],

        initialize: function () {
            this._super();
        },

        afterRenderHook: function () {
            let self = this,
                index;

            self.disableAllSteps();

            if (window.location.hash) {
                var hash = window.location.hash.substring(1);
                switch (hash) {
                    case 'shipping':
                        index = self.shippingStepIndex;
                        self.shippingStepActive(true);
                        break;
                    case 'payment':
                        index = self.paymentStepIndex;
                        self.paymentStepActive(true);
                        break;
                    case 'order-review-step':
                        index = self.orderReviewStepIndex;
                        self.orderReviewStepActive(true);
                        self.shippingStepProcessed(true);
                        break;
                    default:
                        console.log('Could not resolve Checkout Step.');
                        break;
                }
            } else {
                index = self.shippingStepIndex;
                self.shippingStepActive(true);
            }

            $(document).ready(function() {
                $(self.accardionId).accordion({
                    'openedState': 'active',
                    'collapsible': true,
                    'active': [index],
                    'multipleCollapsible': false,
                    'heightStyle': 'content'
                });
            });
        },

        disableAllSteps: function () {
            let self = this;

            self.shippingStepActive(false);
            self.paymentStepActive(false);
            self.orderReviewStepActive(false);
            self.shippingStepProcessed(false);

            //TODO: This is a workaround to controll accardion via the code.
            $.each(self.mapStepIndexToId, function (i, item) {
                $(item).removeClass('active');
                $(item + '-content').hide();
            });
        },

        activateShippingStep: function () {
            let self = this;

            self.disableAllSteps();
            self.shippingStepActive(true);
            self.activateTab(self.shippingStepIndex);
            self.navigateToShippingStep();
            self.loaderManager();
        },

        activatePaymentStep: function () {
            let self = this;

            self.disableAllSteps();
            self.paymentStepActive(true);
            self.activateTab(self.paymentStepIndex);
            self.navigateToPaymentStep();
            self.loaderManager();
        },

        activateOrderReviewStep: function () {
            let self = this;

            self.disableAllSteps();
            self.orderReviewStepActive(true);
            self.activateTab(self.orderReviewStepIndex);
            self.navigateToOrderReviewStep();
            self.loaderManager();
        },

        loaderManager: function (stop = true) {
            if (stop) {
                $('body').loader('hide');
            } else {
                $('body').loader('show');
            }
        },

        activateTab: function (index) {
            let self = this,
                id = self.mapStepIndexToId[index];

            // $(self.accardionId).accordion({
            //     'openedState': 'active',
            //     'collapsible': true,
            //     'active': [index],
            //     'multipleCollapsible': false,
            //     'heightStyle': 'content'
            // });

            // $(self.accardionId).accordion('deactivate');
            $(self.accardionId).accordion({ active: index});
            // $(self.accardionId).accordion('activate', index);


            //TODO: This is a workaround to controll accardion via the code. The approaches above does not work.
            $(id).addClass('active');
            $(id + '-content').show();
        },

        editShipping: function() {
            this.activateShippingStep();
        },

        editPayments: function() {
            this.activatePaymentStep(true);
        },

        navigateToShippingStep: function() {
            window.location.hash = 'shipping';
        },

        navigateToPaymentStep: function() {
            window.location.hash = 'payment';
        },

        navigateToOrderReviewStep: function() {
            window.location.hash = 'order-review-step';
        }
    });
});

<?php

namespace Beside\Checkout\ViewModel;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class CheckoutConfig implements ArgumentInterface
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        try {
            return $this->storeManager->getStore()->getCode();
        } catch (NoSuchEntityException $exception) {
            return $this->storeManager->getDefaultStoreView()->getCode();
        }
    }
}

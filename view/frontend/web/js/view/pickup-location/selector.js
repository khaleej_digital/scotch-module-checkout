define([
    'jquery',
    'ko',
    'Magento_Ui/js/form/form',
    'Beside_Checkout/js/action/stores',
    'Magento_Checkout/js/model/full-screen-loader',
    'Beside_Checkout/js/model/pick-up-form-customer-data',
    'mage/translate'
], function ($,
             ko,
             Component,
             storeResolution,
             fullScreenLoader,
             pickUpFormCustomerData,
             $t
) {
    'use strict';

    return Component.extend({
        isSearching: ko.observable(true),
        searchResults: ko.observableArray([]),
        customerAddress: pickUpFormCustomerData,

        /**
         * Init component
         */
        initialize: function () {
            this._super();
            let self = this;

            self.initPickupLocations();
        },

        initPickupLocations: function () {
            let self = this;
            fullScreenLoader.startLoader();

            storeResolution.getStores()
                .done(
                    function (response) {
                        $.each(response.items, function (i, item) {
                            let location = {
                                code: 1,
                                location: item,
                                selected: ko.observable(false)
                            };
                            self.searchResults.push(location);
                        });

                        self.customerAddress.locations(self.searchResults());

                        fullScreenLoader.stopLoader();
                    }
                ).error(
                    function (response) {
                    console.error(response);
                    self.isSearching(false);
                    fullScreenLoader.stopLoader();
                }
            );
        }
    });
});

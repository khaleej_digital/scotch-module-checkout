define([
    'mage/utils/wrapper',
    'Beside_Checkout/js/view/steps-wrapper'
], function (wrapper, stepsWrapper) {
    'use strict';

    return function (stepNavigator) {
        stepNavigator.setHash = wrapper.wrapSuper(stepNavigator.setHash, function (hash) {
            this._super(hash);

            switch (hash) {
                case 'shipping':
                    stepsWrapper().activateShippingStep();
                    break;
                case 'payment':
                    stepsWrapper().activatePaymentStep();
                    break;
                case 'order-review-step':
                    stepsWrapper().activateOrderReviewStep();
                    break;
                default:
                    console.log('Could not resolve Checkout Step.');
            }
        });

        stepNavigator.navigateTo = wrapper.wrapSuper(stepNavigator.navigateTo, function (code, scrollToElementId) {
            var self = this,
                sortedItems = stepNavigator.steps().sort(this.sortItems);

            sortedItems.forEach(function (element) {
                if (element.code == code) { //eslint-disable-line eqeqeq
                    element.isVisible(true);
                    self.setHash(code);
                } else {
                    element.isVisible(false);
                }
            })
        });

        return stepNavigator;
    };
});

<?php
declare(strict_types=1);

namespace Beside\Checkout\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class SetConfirmationEmailConfig
 *
 * @package Beside\Setup\Patch
 */
class SetConfirmationEmailConfig implements DataPatchInterface
{
    /**
     * @var Config
     */
    private Config $resourceConfig;

    /**
     * SetConfirmationEmailConfig constructor.
     *
     * @param Config $resourceConfig
     */
    public function __construct(
        Config $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * Set send email config "false" to prevent sending confirmation emails before payment
     *
     * @return PatchInterface
     */
    public function apply(): PatchInterface
    {
        $this->resourceConfig->saveConfig(
            \Magento\Sales\Model\Order\Email\Container\OrderIdentity::XML_PATH_EMAIL_ENABLED,
            0,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT
        );

        return $this;
    }

    /**
     * Get dependencies for the patch.
     *
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }
}

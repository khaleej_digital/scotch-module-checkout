<?php
declare(strict_types=1);

namespace Beside\Checkout\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order\Email\Container\OrderIdentity;

/**
 * Class FixConfirmationEmailConfig
 *
 * @package Beside\Checkout\Setup\Patch
 */
class FixConfirmationEmailConfig implements DataPatchInterface
{
    /**
     * @var Config
     */
    private Config $resourceConfig;

    /** @var string Path to send email Paytabs config */
    public const XML_PATH_PAYTABS_EMAIL_CONFIG = 'payment/creditcard/send_invoice';

    /**
     * SetConfirmationEmailConfig constructor.
     *
     * @param Config $resourceConfig
     */
    public function __construct(
        Config $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * Enable Paytabs email sending after successful payment
     *
     * @return PatchInterface
     */
    public function apply(): PatchInterface
    {
        $this->resourceConfig->saveConfig(
            self::XML_PATH_PAYTABS_EMAIL_CONFIG,
            1,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT
        );
        $this->resourceConfig->saveConfig(
            OrderIdentity::XML_PATH_EMAIL_ENABLED,
            1,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT
        );

        return $this;
    }

    /**
     * Get dependencies for the patch.
     *
     * @return array
     */
    public static function getDependencies(): array
    {
        return [SetConfirmationEmailConfig::class];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }
}

<?php

namespace Beside\Checkout\Block;

use Beside\Checkout\Helper\Data as Helper;
use Beside\Erp\Api\StockAggregateApiInterface;
use Magento\Checkout\Model\CompositeConfigProvider;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Cart;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Redbox\Shopfinder\Model\ResourceModel\Shop\CollectionFactory;
use Psr\Log\LoggerInterface;

class Onepage extends \Magento\Checkout\Block\Onepage
{
    const JS_LAYOUT_BESIDE_ONE_STEP = 'jsLayoutOneStep';

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var array|mixed
     */
    protected $jsLayoutOneStep;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManagerInterface;

    /**
     * @var Session
     */
    private $quoteSession;

    /**
     * @var StockAggregateApiInterface
     */
    private $stockAggregateApi;

    /**
     * @var LoggerInterface
     */
    private $loggerInterface;

    /**
     * @var Cart
     */
    private $cart;

    /**
     * @var ManagerInterface
     */
    private $message;

    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * Onepage constructor.
     * @param Context $context
     * @param FormKey $formKey
     * @param CompositeConfigProvider $configProvider
     * @param Helper $helper
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManagerInterface
     * @param Session $quoteSession
     * @param StockAggregateApiInterface $stockAggregateApi
     * @param LoggerInterface $loggerInterface
     * @param Cart $cart
     * @param UrlInterface $url
     * @param ResponseFactory $responseFactory
     * @param ManagerInterface $message
     * @param CartRepositoryInterface $cartRepository
     * @param array $layoutProcessors
     * @param array $data
     * @param Json|null $serializer
     * @param SerializerInterface|null $serializerInterface
     */
    public function __construct(
        Context $context,
        FormKey $formKey,
        CompositeConfigProvider $configProvider,
        Helper $helper,
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManagerInterface,
        Session $quoteSession,
        StockAggregateApiInterface $stockAggregateApi,
        LoggerInterface $loggerInterface,
        Cart $cart,
        UrlInterface $url,
        ResponseFactory $responseFactory,
        ManagerInterface $message,
        CartRepositoryInterface $cartRepository,
        array $layoutProcessors = [],
        array $data = [],
        Json $serializer = null,
        SerializerInterface $serializerInterface = null
    ) {
        parent::__construct($context, $formKey, $configProvider, $layoutProcessors, $data, $serializer, $serializerInterface);
        $this->jsLayoutOneStep = (isset($data[self::JS_LAYOUT_BESIDE_ONE_STEP]) && is_array($data[self::JS_LAYOUT_BESIDE_ONE_STEP]))
            ? $data[self::JS_LAYOUT_BESIDE_ONE_STEP]
            : [];
        $this->serializer = $serializer;
        $this->helper = $helper;
        $this->collectionFactory = $collectionFactory;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->quoteSession = $quoteSession;
        $this->stockAggregateApi = $stockAggregateApi;
        $this->loggerInterface = $loggerInterface;
        $this->cart = $cart;
        $this->url = $url;
        $this->responseFactory = $responseFactory;
        $this->message = $message;
        $this->cartRepository = $cartRepository;

    }

    /**
     * @inheritdoc
     */
    public function getJsLayout()
    {
        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }

        $storeId = $this->storeManagerInterface->getStore()->getId();

        $enabled = $this->helper->getEnabled(
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );

        if ($enabled) {
            $this->jsLayout = array_replace_recursive($this->jsLayout, $this->jsLayoutOneStep);
        }

        $stockAggregateCheckEnabled = $this->helper->geStockAggregateCheckEnabled(
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );

        if ($stockAggregateCheckEnabled) {
            try {
                $this->stockAgragateCheck($storeId);
            } catch (\Exception $exception) {
                $this->loggerInterface->alert(__($exception->getMessage()));
            }
        }

        return $this->serializer->serialize($this->jsLayout);
    }

    /**
     * @param int $storeId
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function stockAgragateCheck($storeId)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteSession->getQuote();
        $unavailableSkus = [];
        $partitialyAvailableItems = [];
        $availableItems = [];
        $errorMessage = '';

        /** @var \Magento\Quote\Api\Data\CartItemInterface $item */
        /*foreach ($quote->getItems() as $item) {
            $sku = $item->getSku();
            $itemId = $item->getItemId();
            $qty = $item->getQty();

            if ($sku !== null) {
                $requestData = $this->stockAggregateApi->prepareData($sku);
                $request = $this->stockAggregateApi->sendRequest(
                    $requestData,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                    $storeId
                );

                if (isset($request['response']) && isset($request['response']['value'])) {
                    if (strpos($request['response']['value'], $sku . ' not found') !== false) {
                        $this->removeQuoteItem($itemId);
                        $unavailableSkus[] = $sku;
                        $this->loggerInterface->error(__($request['response']['value']));
                    }

                    $value = json_decode($request['response']['value'], true);

                    if (isset($value['items']) && isset($value['items'][0]) && isset($value['items'][0]['quantity'])) {
                        $qtyFromResponse = (int) $value['items'][0]['quantity'];

                        if ($qtyFromResponse === 0) {
                            $unavailableSkus[] = $sku;
                            $this->removeQuoteItem($itemId);
                        } else if ($qty > $qtyFromResponse) {
                            $item->setQty($qtyFromResponse);
                            $partitialyAvailableItems[$sku] = $item;
                        } else {
                            $availableItems[] = $item;
                        }
                    }
                } else {
                    $errorMessage = __('Stock Check API return not valid response.');
                    $this->loggerInterface->error($errorMessage);
                }
            }
        }

        if (count($unavailableSkus)) {
            $errorMessage .= __('Some Item(s) are not available and removed from the cart. SKU(s): %1.',
                implode(', ', $unavailableSkus)
            ) . ' ';
        }

        if (count($partitialyAvailableItems)) {
            $updatedItems = array_merge($availableItems, $partitialyAvailableItems);
            $quote->setItems($updatedItems);
            $this->cartRepository->save($quote);

            $errorMessage .= __('Some Item(s) are partitialy available. Quantity for such item(s) updated. SKU(s): %1',
                implode(', ', array_keys($partitialyAvailableItems))
            );
        }

        if ($errorMessage !== '') {
            $this->redirectToCartPage($errorMessage);
        }*/
    }

    /**
     * @param $itemId
     */
    private function removeQuoteItem($itemId)
    {
        $this->cart->removeItem($itemId)->save();
    }

    /**
     * @return void
     */
    private function addShopsDropDownComponent()
    {
        $this->jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['pickup-location']['children']['pickup-location-selector']['children']['shop-drop-down'] = [
            'component' => 'Magento_Ui/js/form/element/select',
            'config' => [
                'customScope' => 'shippingAddress',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
                'id' => 'shop-drop-down',
            ],
            'dataScope' => 'shippingAddress.shop-drop-down',
            'label' => __('Order Online, Pick Up In Store With Free Sheeping!'),
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => [],
            'sortOrder' => 10,
            'id' => 'shop-drop-down',
            'options' => $this->getShopList()
        ];
    }

    /**
     * @param $message
     */
    private function redirectToCartPage($message)
    {
        $this->message->addErrorMessage($message);
        $customRedirectionUrl = $this->url->getUrl('checkout/cart');
        $this->responseFactory->create()
            ->setRedirect($customRedirectionUrl)
            ->sendResponse();
        exit();
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getShopList()
    {
        $result = [
            [
                'value' => null,
                'label' => __('Please select a shop')
            ]
        ];

        /** @var  $collection \Redbox\Shopfinder\Model\ResourceModel\Shop\Collection */
        $collection = $this->collectionFactory->create();
        $collection->addStoreFilter(
            $this->storeManagerInterface->getStore(),
            false
        )->addFilter(
            'status',
            1
        );

        /** @var \Redbox\Shopfinder\Api\Data\ShopInterface  $item */
        foreach ($collection->getItems() as $item) {
            $result[] = [
                'value' => $item->getId(),
                'label' => __($item->getShopName())
            ];
        }

        return $result;
    }
}

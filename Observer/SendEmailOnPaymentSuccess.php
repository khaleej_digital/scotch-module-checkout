<?php
declare(strict_types=1);

namespace Beside\Checkout\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class SendEmailOnPaymentSuccess
 *
 * @package Beside\Checkout\Observer
 */
class SendEmailOnPaymentSuccess implements ObserverInterface
{
    /**
     * XML path to confirmation email config
     */
    public const XML_PATH_SALES_EMAIL = 'beside_checkout/sales_email/enabled';

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * SendEmailOnPaymentSuccess constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Prevent sending email before payment
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var OrderInterface $order */
        $order = $observer->getEvent()->getOrder();

        $afterPaymentFlag = $this->scopeConfig->getValue(self::XML_PATH_SALES_EMAIL);
        if ($afterPaymentFlag) {
            $payment = $order->getPayment();
            if ($payment) {
                $method = $payment->getMethod();
                if ($method == \PayTabs\PayPage\Model\Ui\ConfigProvider::CODE_CREDITCARD) {
                    $order->setCanSendNewEmailFlag(false);
                }
            }
        }
    }
}

<?php

namespace Beside\Checkout\Model\Search;

use Beside\Checkout\Api\Search\SearchCriteriaInterface;

class SearchCriteria extends \Redbox\Shopfinder\Api\Search\SearchCriteria implements SearchCriteriaInterface
{
    /**
     * @param int $radiusDistance
     * @return SearchCriteria|SearchCriteriaInterface
     */
    public function setRadiusDistance($radiusDistance)
    {
        return $this->setData(self::RADIUS_DISTANCE, $radiusDistance);
    }

    /**
     * @return int|mixed|null
     */
    public function getRadiusDistance()
    {
        return $this->_get(self::RADIUS_DISTANCE);
    }

    /**
     * @param int $shopId
     * @return SearchCriteria|SearchCriteriaInterface
     */
    public function setShopId($shopId)
    {
        return $this->setData(self::SHOP_ID, $shopId);
    }

    /**
     * @return int|mixed|null
     */
    public function getShopId()
    {
        return $this->_get(self::SHOP_ID);
    }
}

<?php

namespace Beside\Checkout\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Data extends AbstractHelper
{
    const XML_PATH_BESIDE_CHECKOUT_GENERAL_ENABLED = 'beside_checkout/general/enabled';

    const XML_PATH_BESIDE_CHECKOUT_GENERAL_STOCK_CHECK_ENABLED = 'beside_checkout/general/stock_aggregate_check_enabled';

    /**
     * @param string $scopeType
     * @param string|null $scopeCode
     * @return bool
     */
    public function getEnabled($scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_PATH_BESIDE_CHECKOUT_GENERAL_ENABLED,
            $scopeType,
            $scopeCode
        );
    }

    /**
     * @param string $scopeType
     * @param string|null $scopeCode
     * @return bool
     */
    public function geStockAggregateCheckEnabled($scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_PATH_BESIDE_CHECKOUT_GENERAL_STOCK_CHECK_ENABLED,
            $scopeType,
            $scopeCode
        );
    }
}

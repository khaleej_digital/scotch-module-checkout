<?php

namespace Beside\Checkout\Plugin;

use Magento\Sales\Model\Order;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Redbox\Shopfinder\Model\Geocoder;
use Beside\Checkout\Model\Carrier\StorePickup;

class CoordinatesResolver
{
    /**
     * @var Geocoder
     */
    private $geocoder;

    /**
     * @var StorePickup
     */
    private $storePickup;

    /**
     * CoordinatesResolver constructor.
     * @param Geocoder $geocoder
     * @param StorePickup $storePickup
     */
    public function __construct(
        Geocoder $geocoder,
        StorePickup $storePickup
    ) {
        $this->geocoder = $geocoder;
        $this->storePickup = $storePickup;
    }

    /**
     * @param \Magento\Sales\Api\OrderManagementInterface $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     */
    public function beforePlace(
        \Magento\Sales\Api\OrderManagementInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $order
    ) {
        if ($order instanceof Order && $order->getShippingAddress()
            && strpos($order->getShippingMethod(), $this->storePickup->getShippingMethodCode()) === false
        ) {
            /** @var \Magento\Sales\Model\Order\Address $address */
            $address = $order->getShippingAddress();
            $coordinates = $this->getCustomerCoordinates($address);

            if (!empty($coordinates)) {
                $order->setLongitude($coordinates['longitude']);
                $order->setLatitude($coordinates['latitude']);
            }
        }
    }

    /**
     * TODO: code duplicates in Beside\Checkout\Plugin\SaveAddress::getCustomerCoordinates().
     * It has to be place in a single place, e.g. CoordinatesResolverModel
     *
     * @param OrderAddressInterface $address
     * @return array|Geocoder
     * @throws \Zend_Json_Exception
     */
    private function getCustomerCoordinates(OrderAddressInterface $address)
    {
        $result = [];
        $address = str_replace(' ', '+', $address->getCity()) . ',' . $address->getCountryId();
        $response = $this->geocoder->fetchCoordinates($address, true);

        if (is_array($response) && isset($response['latitude']) && isset($response['longitude'])) {
            $result = $response;
        }

        return $result;
    }
}

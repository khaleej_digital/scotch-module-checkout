define([
        'jquery'
    ], function ($
    ) {
        return {
            getStoresUrl: function () {
                let searchUrl = 'rest/V1/beside_checkout/search';
                let searchCriteria = {
                    'searchCriteria': {
                        'filter_groups': {
                            '0': {
                                'filters': {
                                    '0': {'field': 'status', 'value': 1}
                                }
                            }
                        }
                    }
                };

                searchUrl += '?' + decodeURIComponent($.param(searchCriteria));

                return searchUrl;
            },

            getStoreByIdUrl: function (id) {
                let searchUrl = 'rest/V1/beside_checkout/get-shop-by-id/';

                searchUrl += id;
                return searchUrl;
            },
        }
    }
);

<?php
namespace Beside\Checkout\Plugin\Model;

use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Spi\OrderResourceInterface;
use Magento\Sales\Model\Order\Address\Renderer as AddressRenderer;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Redbox\Shopfinder\Api\ShopRepositoryInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Psr\Log\LoggerInterface;

class OrderPlugin
{
    const READY_FOR_COLLECTION_ORDER_STATUS= 'ready_for_collection';
    const COLLECTED_ORDER_STATUS  = 'collected';

    /**
     * @var OrderInterface
     */
    private $orderInterface;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var StateInterface
     */
    private $inlineTranslation;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var AddressRenderer
     */
    private $addressRenderer;

    /**
     * @var PaymentHelper
     */
    private $paymentHelper;

    /**
     * OrderPlugin constructor.
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $state
     * @param LoggerInterface $logger
     * @param ScopeConfigInterface $scopeConfig
     * @param ShopRepositoryInterface $shopRepository
     * @param StoreManagerInterface $storeManager
     * @param AddressRenderer $addressRenderer
     * @param PaymentHelper $paymentHelper
     */
    public function __construct(
        OrderInterface $orderInterface,
        TransportBuilder $transportBuilder,
        StateInterface $state,
        LoggerInterface $logger,
        ScopeConfigInterface $scopeConfig,
        ShopRepositoryInterface $shopRepository,
        StoreManagerInterface $storeManager,
        AddressRenderer $addressRenderer,
        PaymentHelper $paymentHelper
    ) {
        $this->orderInterface = $orderInterface;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $state;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->shopRepository = $shopRepository;
        $this->storeManager = $storeManager;
        $this->addressRenderer = $addressRenderer;
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * @param \Magento\Sales\Model\Order $subject
     * @param \Magento\Sales\Model\Order $order
     * @return \Magento\Sales\Model\Order
     */
    public function afterSave(OrderRepositoryInterface $subject, OrderInterface $resultOrder)
    {
        if ($resultOrder->getBillingAddress()->getPickupLocationId()){
            if ($resultOrder->getStatus() === self::READY_FOR_COLLECTION_ORDER_STATUS) {
                $order = $this->orderInterface->load($resultOrder->getEntityId());
                $this->sendReadyForCollectionEmail($order);
            }

            if ($resultOrder->getStatus() === self::COLLECTED_ORDER_STATUS) {
                $order = $this->orderInterface->load($resultOrder->getEntityId());
                $this->sendCollectedEmail($order);
            }
        }

        return $resultOrder;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     */
    private function sendReadyForCollectionEmail(\Magento\Sales\Model\Order $order)
    {
        $this->sendEmail($order, self::READY_FOR_COLLECTION_ORDER_STATUS);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     */
    private function sendCollectedEmail(\Magento\Sales\Model\Order $order)
    {
        $this->sendEmail($order, self::COLLECTED_ORDER_STATUS);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     */
    private function sendEmail(\Magento\Sales\Model\Order $order, string $orderStatus)
    {
        $storeId = $order->getStoreId();
        $toEmail = $order->getCustomerEmail();
        $fromEmail = $this->scopeConfig->getValue(
            'trans_email/ident_sales/email',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $fromName = $this->scopeConfig->getValue(
            'trans_email/ident_sales/name',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        if ($order->getCustomerId() === null) {
            $templateId = $orderStatus === self::READY_FOR_COLLECTION_ORDER_STATUS
                ? $this->scopeConfig->getValue(
                    'sales_email/order_ready_for_pickup/guest_template', ScopeInterface::SCOPE_STORE, $storeId)
                : $this->scopeConfig->getValue(
                    'sales_email/order_collected/guest_template', ScopeInterface::SCOPE_STORE, $storeId);
        } else {
            $templateId = $orderStatus === self::READY_FOR_COLLECTION_ORDER_STATUS
                ? $this->scopeConfig->getValue(
                    'sales_email/order_ready_for_pickup/template', ScopeInterface::SCOPE_STORE, $storeId)
                : $this->scopeConfig->getValue(
                    'sales_email/order_collected/template', ScopeInterface::SCOPE_STORE, $storeId);
        }

        try {
            $templateVars = $this->prepareTemplateVars($order);
            $from = ['email' => $fromEmail, 'name' => $fromName];
            $templateOptions = [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $storeId
            ];

            $this->inlineTranslation->suspend();

            $transport = $this->transportBuilder->setTemplateIdentifier($templateId)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($toEmail)
                ->getTransport();
            $transport->sendMessage();

            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }

    /**
     * @param $shopId
     * @return \Redbox\Shopfinder\Api\Data\ShopInterface
     */
    private function getShopData($shopId)
    {
        try {
            /** @var \Redbox\Shopfinder\Api\Data\ShopInterface $shop */
            $shop = $this->shopRepository->getById($shopId);

            return [
                'store_email' => (string) $shop->getShopEmail(),
                'store_phone' => (string) $shop->getShopPhone(),
                'store_hours' => (string) $shop->getOpenTimes(),
                'pickupAddress' => (string) $shop->getShopName() . ' ' . $shop->getCountryId() . ' ' . $shop->getStreetFull()
            ];
        } catch (\Exception $exception) {
            $this->logger->error(__('The Shopfinder Entity could not be loaded. Exception: %1', $exception->getMessage()));
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function prepareTemplateVars(\Magento\Sales\Model\Order $order)
    {
        $storeId = $order->getStoreId();
        $shopId = $order->getBillingAddress()->getPickupLocationId();
        $shopData = $this->getShopData($shopId);

        $templateVars = [
            'order' => $order,
            'store' => $this->storeManager->getStore($storeId),
            'formattedShippingAddress' => $this->addressRenderer->format(
                $order->getShippingAddress(),
                'html'
            ),
            'formattedBillingAddress' => $this->addressRenderer->format(
                $order->getBillingAddress(),
                'html'
            ),
            'payment_html' => $this->paymentHelper->getInfoBlockHtml($order->getPayment(), $storeId),
            'shipping_msg' => '' //TODO: Clarify what has to be adde here?
        ];

        return array_merge($templateVars, $shopData);
    }
}

var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/model/step-navigator': {
                'Beside_Checkout/js/model/beside-step-navigator-mixin': true
            },
            'Magento_OfflinePayments/js/view/payment/method-renderer/checkmo-method': {
                'Beside_Checkout/js/view/payment/method-renderer/checkmo-method-mixin': true
            },
            'Magento_OfflinePayments/js/view/payment/method-renderer/cashondelivery-method': {
                'Beside_Checkout/js/view/payment/method-renderer/cashondelivery-method-mixin': true
            },
            'PayTabs_PayPage/js/view/payment/method-renderer/paypage-method': {
                'Beside_Checkout/js/view/payment/method-renderer/paypage-method-mixin': true
            }
        }
    }
}

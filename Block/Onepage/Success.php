<?php

namespace Beside\Checkout\Block\Onepage;

use Magento\Customer\Model\Context;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Model\Order;
use Redbox\Shopfinder\Model\ResourceModel\Shop\Collection;

class Success extends \Magento\Checkout\Block\Onepage\Success
{

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $orderAddress;

    /**
     * @var \Redbox\Shopfinder\Model\ResourceModel\Shop\Collection
     */
    private $shopCollection;

    /**
     * @var Product
     */
    public $product;

    /**
     * @var Order
     */
    public $order;

    /**
     * @var CountryFactory
     */
    private $countryFactory;

    /**
     * @var ImageBuilder
     */
    private $imageHelper;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var String
     */
    private $shopName = '';
    private $shopStreet = '';
    private $postcode = '';
    private $city = '';
    private $countryName = '';

    /** @var \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet **/
    protected $attributeSet;

    const ATTRIBUTE_SET_NAME_VALUE = "DENIM";

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Sales\Api\Data\OrderAddressInterface $orderAddress,
        \Redbox\Shopfinder\Model\ResourceModel\Shop\Collection $shopCollection,
        \Magento\Catalog\Model\Product $product,
        \Magento\Sales\Model\Order $order,
        \Magento\Directory\Model\CountryFactory $country,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        parent::__construct($context, $checkoutSession,$orderConfig,$httpContext,$data);
        $this->orderAddress = $orderAddress;
        $this->shopCollection = $shopCollection;
        $this->product = $product;
        $this->order = $order;
        $this->attributeSet = $attributeSet;
        $this->countryFactory = $country;
        $this->imageHelper = $imageHelper;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @param $shippingAddress
     */
    public function getShopDetails($shippingAddress)
    {
        $shopName = $shippingAddress->getPickupLocationId();
        $this->shopCollection->addFieldToFilter('shop_id', $shopName);
        $this->shopCollection->getSelect()->limit(1);
        $this->shopName= $this->shopCollection->getFirstItem()->getShopName();
        $this->shopStreet = $this->shopCollection->getFirstItem()->getStreet();
        $this->postcode= $this->shopCollection->getFirstItem()->getPostcode();
        $this->city= $this->shopCollection->getFirstItem()->getCity();
        $this->countryName= $this->shopCollection->getFirstItem()->getCountryName();
    }

    /**
     * @param $product
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAttributeSetName($product) {
        $attributeSetRepository = $this->attributeSet->get($product->getAttributeSetId());
        return $attributeSetRepository->getAttributeSetName();
    }

    /**
     * @param string $countryCode
     * @return string
     */
    public function getCountryByCode(string $countryCode): string
    {
        /** @var \Magento\Directory\Model\Country $country */
        $country = $this->countryFactory->create();
        return $country->loadByCode($countryCode)->getName();
    }

    /**
     * Retrieve product image
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getImage($product)
    {
        return $this->imageHelper->init($product, 'product_thumbnail_image')->getUrl();
    }

    /**
     * result
     *
     * @return string
     */
    public function formatPrice($price)
    {
        return $this->priceCurrency->format(
            $price,
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $this->_storeManager->getStore()
        );
    }

    /**
     * @return mixed
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * @return mixed
     */
    public function getShopStreet()
    {
        return $this->shopStreet;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getCountryName()
    {
        return $this->countryName;
    }
}

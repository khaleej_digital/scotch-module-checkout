Beside Module Checkout
# Changelog
* 1.2.0 Add Observer, add configs to send email after success payment
* 1.0.0 initial version of module: add order extension attribute Delivery Date 
* 1.7.3 Gift Wrapping feature on shipping address
* 1.8.2 checkout/login page added. Continue as a guest or login (email is hidden in checkout step)

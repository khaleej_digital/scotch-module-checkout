<?php
/**
 * @category  Beside
 * @package   Beside_Checkout
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */


namespace Beside\Checkout\Controller\Login;

/**
 * Override for checkout/cart url
 *
 * Class Index
 * @package Beside\Checkout\Controller\Login
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->pageFactory = $pageFactory;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if($this->customerSession->isLoggedIn()){
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setUrl($this->_url->getUrl('checkout'));

            return $resultRedirect;
        }

        $resultPage = $this->pageFactory->create();

        return $resultPage;
    }
}

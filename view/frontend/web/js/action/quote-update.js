define(
    [
        'mage/storage',
        'Beside_Checkout/js/model/quote'
    ],
    function (storage, quote) {
        'use strict';

        return {
            shippingInformation: function (quoteId, shippingAddress, billingAddress, carrierCode, methodCode) {
                return storage.post(
                    quote.shippingInformationUpdate(quoteId),
                    JSON.stringify(quote.buildShippingInformationUpdatePayload(shippingAddress, billingAddress, carrierCode, methodCode))
                );
            },
            
            carrierInformation: function (quoteId, carrierCode, methodCode) {
                return storage.post(
                    quote.shippingInformationUpdate(quoteId),
                    JSON.stringify(quote.buildCarrierUpdatePayload(carrierCode, methodCode))
                );
            }
        };
    }
);

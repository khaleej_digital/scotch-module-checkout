<?php

namespace Beside\Checkout\Observer\Order;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class EmailTemplateVars implements ObserverInterface
{
    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $ShpMethod = $observer->getEvent()->getTransport();

        if($ShpMethod->getOrder()->getshippingmethod() == 'beside_store_pickup_beside_store_pickup')
        {
            $ShpMethod['shippingMethod']  = 'Store Pickup';
        }
        else{
            $ShpMethod['shippingMethod']  = '';
        }
    }
}

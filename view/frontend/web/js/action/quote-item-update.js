define([
    'mage/storage',
    'Beside_Checkout/js/model/quote'
], function (storage, quote) {
    'use strict';

    return {
        update: function (quoteId, itemId, qty) {
            return storage.post(
                quote.itemUpdate(quoteId),
                JSON.stringify(quote.buildItemUpdatePayload(quoteId, itemId, qty))
            );
        },

        delete: function (quoteId, itemId) {
            return storage.delete(quote.itemDelete(quoteId, itemId));
        }
    };
});

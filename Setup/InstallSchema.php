<?php

namespace Beside\Checkout\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var SchemaSetupInterface
     */
    private $setup;

    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    private $salesSetupFactory;

    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    public function __construct(
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws LocalizedException
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;
        $this->setup->startSetup();

        $this->addPickupLocationIdColumn();

        $this->setup->endSetup();
    }

    /**
     * @throws LocalizedException
     */
    private function addPickupLocationIdColumn()
    {
        $salesSetup = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $this->moduleDataSetup]);
        $quoteSetup = $this->quoteSetupFactory->create(['resourceName' => 'quote_setup', 'setup' => $this->moduleDataSetup ]);

        /**
         * Add attributes
         */
        $quoteSetup->addAttribute(
            'quote_address',
            'pickup_location_id',
            ['type' => Table::TYPE_TEXT, 'visible' => false]
        );

        $salesSetup->addAttribute(
            'order_address',
            'pickup_location_id',
            ['type' => Table::TYPE_TEXT, 'visible' => false]
        );
    }
}

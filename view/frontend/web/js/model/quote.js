define([
    'Magento_Customer/js/model/customer'
], function (customer) {
    return {
        itemUpdate: function (quoteId) {
            if (customer.isLoggedIn()) {
                return 'rest/' + window.checkoutConfig.storeCode + '/V1/carts/mine/items';
            }
            return 'rest/' + window.checkoutConfig.storeCode + '/V1/guest-carts/' + quoteId + '/items';

        },
        
        buildItemUpdatePayload: function (quoteId, itemId, qty) {
            return {
                'cartItem': {
                    'quote_id': quoteId,
                    'item_id': itemId,
                    'qty': qty,
                }
            };
        },

        itemDelete: function (quoteId, itemId) {
            if (customer.isLoggedIn()) {
                return 'rest/' + window.checkoutConfig.storeCode + '/V1/carts/mine/items/' + itemId;
            }
            return 'rest/' + window.checkoutConfig.storeCode + '/V1/guest-carts/' + quoteId + '/items/' + itemId;

        },

        shippingInformationUpdate: function (quoteId) {
            if (customer.isLoggedIn()) {
                return 'rest/' + window.checkoutConfig.storeCode + '/V1/carts/mine/shipping-information';
            }
            return 'rest/' + window.checkoutConfig.storeCode + '/V1/guest-carts/' + quoteId + '/shipping-information';

        },

        buildCarrierUpdatePayload: function (carrierCode, methodCode) {
            return {
                'addressInformation': {
                    'shipping_carrier_code': carrierCode,
                    'shipping_method_code': methodCode
                }
            };
        },

        buildShippingInformationUpdatePayload: function (shippingAddress, billingAddress, carrierCode, methodCode) {
            return {
                'addressInformation': {
                    'shipping_address': shippingAddress,
                    'billing_address': billingAddress,
                    'shipping_carrier_code': carrierCode,
                    'shipping_method_code': methodCode
                }
            };
        },

        buildMultiShippingInformationUpdatePayload: function (billingAddress, dcShippingAddress, dcCarrierCode, dcMethodCode, alternativeShippingAddress, alternativeCarrierCode, alternativeMethodCode) {
            return {
                'addressInformation': {
                    'billing_address': billingAddress,
                    'dc_shipping_address': dcShippingAddress,
                    'dc_shipping_carrier_code': dcCarrierCode,
                    'dc_shipping_method_code': dcMethodCode,
                    'alternative_shipping_address': alternativeShippingAddress,
                    'alternative_shipping_carrier_code': alternativeCarrierCode,
                    'alternative_shipping_method_code': alternativeMethodCode
                }
            };
        }
    }
});

<?php

namespace Beside\Checkout\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;

class StorePickup extends AbstractCarrier implements CarrierInterface
{
    /**
     * @var string
     */
    protected $code = 'beside_store_pickup';

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $rateMethodFactory;

    /**
     * @var \Redbox\Shopfinder\Model\ShopRepository
     */
    protected $shopRepository;

    /**
     * @var \Redbox\Shopfinder\Api\Search\SearchCriteriaInterface
     */
    protected $shopSearchCriteria;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Redbox\Shopfinder\Model\ShopRepository $shopRepository
     * @param \Redbox\Shopfinder\Api\Search\SearchCriteriaInterface $shopSearchCriteria
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Redbox\Shopfinder\Model\ShopRepository $shopRepository,
        \Redbox\Shopfinder\Api\Search\SearchCriteriaInterface $shopSearchCriteria,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->shopRepository = $shopRepository;
        $this->shopSearchCriteria = $shopSearchCriteria;
        $this->_code = $this->code;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @return bool|\Magento\Framework\DataObject|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function collectRates(RateRequest $request)
    {
        $request->getStoreId();
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        $result = $this->rateResultFactory->create();
        $method = $this->rateMethodFactory->create();

        $method->setCarrier($this->code);
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod($this->code);
        $method->setMethodTitle($this->getConfigData('shipping_method_title'));
        $method->setPrice($this->getShippingPrice($request, $this->getFreeBoxesCount($request)));
        $method->setCost(0);
        $result->append($method);

        return $result;
    }

    /**
     * @param RateRequest $request
     * @param int $freeBoxes
     * @return bool|float
     */
    private function getShippingPrice(RateRequest $request, $freeBoxes)
    {
        if ($request->getFreeShipping()) {
            return '0.00';
        }

        $price = $this->getConfigData('price');

        if ($price !== false && $request->getPackageQty() == $freeBoxes) {
            $price = '0.00';
        }
        return $price;
    }

    /**
     * @param RateRequest $request
     * @return int
     */
    private function getFreeBoxesCount(RateRequest $request)
    {
        $freeBoxes = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    $freeBoxes += $this->getFreeBoxesCountFromChildren($item);
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
        return $freeBoxes;
    }

    /**
     * @param mixed $item
     * @return mixed
     */
    private function getFreeBoxesCountFromChildren($item)
    {
        $freeBoxes = 0;
        foreach ($item->getChildren() as $child) {
            if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                $freeBoxes += $item->getQty() * $child->getQty();
            }
        }
        return $freeBoxes;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->code => $this->getConfigData('shipping_method_title')];
    }

    /**
     * @return string
     */
    public function getShippingMethodCode()
    {
        return $this->code;
    }

    /**
     * @return bool
     */
    public function isFixed()
    {
        return false;
    }
}

<?php

namespace Beside\Checkout\Api\Search;

interface SearchCriteriaInterface extends \Redbox\Shopfinder\Api\Search\SearchCriteriaInterface
{
    const RADIUS_DISTANCE = 'radius_distance';
    const SHOP_ID = 'shop_id';

    /**
     * @param integer $radiusDistance
     *
     * @return $this
     */
    public function setRadiusDistance($radiusDistance);

    /**
     * @return integer
     */
    public function getRadiusDistance();

    /**
     * @param integer $shopId
     *
     * @return $this
     */
    public function setShopId($shopId);

    /**
     * @return integer
     */
    public function getShopId();
}

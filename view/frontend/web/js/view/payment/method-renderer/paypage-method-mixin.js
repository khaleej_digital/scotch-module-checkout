define([
    'jquery',
    'ko',
    'mage/utils/wrapper',
    'Beside_Checkout/js/view/steps-wrapper'
], function ($, ko, wrapper, stepsWrapper) {
    'use strict';

    var mixin = {
        defaults: {
            template: 'Beside_Checkout/payment/paypage'
        },

        initialize: function () {
            return this._super();
        },

        proceedToReview: function (data, event) {
            let self = this,
                result;

            if (event) {
                event.preventDefault();
            }

            stepsWrapper().activateOrderReviewStep();
            $('#order-review-step-tab').collapsible('activate')
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});

<?php

namespace Beside\Checkout\Observer\Order;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\InvoiceManagementInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\OfflinePayments\Model\Cashondelivery;
use Psr\Log\LoggerInterface;

class InvoiceForCod implements ObserverInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var InvoiceManagementInterface
     */
    private $invoiceService;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * StatusChange constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param InvoiceManagementInterface $invoiceService
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        InvoiceManagementInterface $invoiceService,
        InvoiceRepositoryInterface $invoiceRepository,
        LoggerInterface $logger
    ) {
        $this->orderRepository = $orderRepository;
        $this->invoiceService = $invoiceService;
        $this->invoiceRepository = $invoiceRepository;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Shipment $shipment */
        $shipment = $observer->getEvent()->getShipment();
        /** @var OrderInterface $order */
        $order = $shipment->getOrder();
        $hasInvoices = $order->hasInvoices();

        if ($order->getPayment()->getMethod() === Cashondelivery::PAYMENT_METHOD_CASHONDELIVERY_CODE && !$hasInvoices) {
            try {
                /** @var \Magento\Sales\Model\Order\Invoice  $invoice */
                $invoice = $this->invoiceService->prepareInvoice($order);
                $invoice->register();
                $this->invoiceRepository->save($invoice);

                //TODO: add or remove this after Joe will clarify this with the client.
                //NOTE: Notify functionality has an issue:
                //TypeError: Argument 1 passed to Magento\Framework\Mail\AddressConverter::convertMany() must be of the type array, null
                //given, called in /Users/developer/Development/beside.loc/vendor/magento/framework/Mail/Template/TransportBuilder.php on
                //line 431 and defined in /Users/developer/Development/beside.loc/vendor/magento/framework/Mail/AddressConverter.php:59
                //$this->invoiceService->notify($invoice->getEntityId());

                $order->addStatusHistoryComment(
                    __('The Invoice was generated automatically. Invoice No. %1', $invoice->getIncrementId())
                );
                $this->orderRepository->save($order);
            } catch (\Exception $exception) {
                $this->logger->error(
                    __('The Invoice for COD Order has not been created, because of the Exception: %1', $exception->getMessage())
                );
            }
        }
    }
}

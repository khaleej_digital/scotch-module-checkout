<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Checkout\Plugin\CustomerData;

/**
 * Class Cart
 * @package Beside\Checkout\Plugin\CustomerData
 */
class Cart
{
    /**
     * @var \Magento\Tax\Model\Config
     */
    private $taxConfig;

    /**
     * Cart constructor.
     * @param \Magento\Tax\Model\Config $taxConfig
     */
    public function __construct(
        \Magento\Tax\Model\Config $taxConfig
    ) {
        $this->taxConfig = $taxConfig;
    }

    /**
     * @param \Magento\Checkout\CustomerData\Cart $subject
     * @param $result
     * @return array
     */
    public function afterGetSectionData(
        \Magento\Checkout\CustomerData\Cart $subject,
        $result
    ) {
        //Below key has to be first element of array, don't know the reason but if its last, its not working for first page load
        $data['display_incl_tax'] = $this->taxConfig->displayCartSubtotalInclTax();
		
		// Haingo 20211005: get size value
		$items = isset($result['items']) ? $result['items'] : [];
		$newItems = [];
		if(sizeof($items)>0){
			foreach($items as $item){
				$size = '';
				$options = isset($item['options']) ? $item['options'] : [];
				if(sizeof($options)>0){
					foreach($options as $option){
						if(isset($option['value']) && isset($option['label']) && ($option['label'] == "Size")){
							$size = '('.$option['value'].')';
						}
					}
				}
				$item['size'] = $size;
				$newItems[] = $item;
			}
		}
		$result['items'] = $newItems;
		// Fin Haingo 20211005
		
        return array_merge($data, $result);
    }
}

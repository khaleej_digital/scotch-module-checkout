<?php
/**
 * @category  Beside
 * @package   Beside_Checkout
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Checkout\Block\Form;
/**
 * Class Guest
 * @package Beside\Checkout\Block\Form
 */
class Guest extends \Magento\Framework\View\Element\Template
{
    /**
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->getUrl('*/*/guest');
    }
}

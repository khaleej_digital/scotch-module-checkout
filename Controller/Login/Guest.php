<?php
/**
 * @category  Beside
 * @package   Beside_Checkout
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Checkout\Controller\Login;

/**
 * Class Guest
 * @package Beside\Checkout\Controller\Login
 */
class Guest extends \Magento\Framework\App\Action\Action
{
    /**
     * Guest constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $this->checkoutSession->getQuote()->setCustomerEmail($params['login']['username'])->save();

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($this->_url->getUrl('checkout'));

        return $resultRedirect;
    }
}

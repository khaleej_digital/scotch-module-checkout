<?php

namespace Beside\Checkout\Model;

use Magento\Customer\Api\Data\RegionInterface;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Redbox\Shopfinder\Api\Data;
use Redbox\Shopfinder\Model\Shop;

class ShopRepositoryPreference implements \Beside\Checkout\Api\ShopRepositoryInterface
{
    /**
     * @var \Redbox\Shopfinder\Model\ResourceModel\Shop
     */
    protected $resource;

    /**
     * @var \Redbox\Shopfinder\Model\ShopFactory
     */
    protected $shopFactory;

    /**
     * @var \Redbox\Shopfinder\Helper\Data
     */
    protected $moduleHelper;

    /**
     * @var \Redbox\Shopfinder\Model\Geocoder
     */
    protected $geocoder;

    /**
     * @var \Redbox\Shopfinder\Model\ResourceModel\Shop\CollectionFactory
     */
    protected $shopCollectionFactory;

    /**
     * @var Data\ShopSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var Data\ShopInterfaceFactory
     */
    protected $dataShopFactory;

    /**
     * @var RegionInterfaceFactory
     */
    protected $regionDataFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Redbox\Shopfinder\Model\ResourceModel\Shop $resource
     * @param \Redbox\Shopfinder\Model\ShopFactory $shopFactory
     * @param \Redbox\Shopfinder\Helper\Data $moduleHelper
     * @param \Redbox\Shopfinder\Model\Geocoder $geocoder
     * @param Data\ShopInterfaceFactory $dataShopFactory
     * @param \Redbox\Shopfinder\Model\ResourceModel\Shop\CollectionFactory $shopCollectionFactory
     * @param Data\ShopSearchResultsInterfaceFactory $searchResultsFactory
     * @param RegionInterfaceFactory $regionDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        \Redbox\Shopfinder\Model\ResourceModel\Shop $resource,
        \Redbox\Shopfinder\Model\ShopFactory $shopFactory,
        \Redbox\Shopfinder\Helper\Data $moduleHelper,
        \Redbox\Shopfinder\Model\Geocoder $geocoder,
        \Redbox\Shopfinder\Api\Data\ShopInterfaceFactory $dataShopFactory,
        \Redbox\Shopfinder\Model\ResourceModel\Shop\CollectionFactory $shopCollectionFactory,
        \Redbox\Shopfinder\Api\Data\ShopSearchResultsInterfaceFactory $searchResultsFactory,
        RegionInterfaceFactory $regionDataFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->shopFactory = $shopFactory;
        $this->moduleHelper = $moduleHelper;
        $this->geocoder = $geocoder;
        $this->regionDataFactory = $regionDataFactory;
        $this->shopCollectionFactory = $shopCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataShopFactory = $dataShopFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Beside\Checkout\Api\Search\SearchCriteriaInterface $criteria
     * @return Data\ShopSearchResultsInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws \Zend_Json_Exception
     */
    public function getList(
        \Beside\Checkout\Api\Search\SearchCriteriaInterface $criteria
    ) {
        /** @var \Redbox\Shopfinder\Api\Data\ShopSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        /** @var  $collection \Redbox\Shopfinder\Model\ResourceModel\Shop\Collection */
        $collection = $this->shopCollectionFactory->create();
        $collection->addStoreFilter(
            $this->storeManager->getStore(),
            true
        );
        $lng = $criteria->getLongitude();
        $lat = $criteria->getLatitude();
        $radiusDistance = $criteria->getRadiusDistance();
        $useRadius = $criteria->getUseRadius();
        if ($lng && $lat) {
            $collection = $this->addLatLonFilter(
                $collection,
                $lat,
                $lng,
                $useRadius,
                $radiusDistance
            );
        }
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $filterValue = $filter->getValue();
                if (in_array(strtolower($condition), ['in', 'nin'])
                    && stripos(
                        $filterValue,
                        ','
                    ) !== false
                ) {
                    $filterValue = explode(',', $filterValue);
                }
                $collection->addFieldToFilter(
                    $filter->getField(),
                    [$condition => $filterValue]
                );
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection()
                        == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $searchResults->setItems($this->buildSearchResultItemsFromCollection($collection));
        return $searchResults;
    }

    /**
     * Load Shop data by given Shop Identity
     *
     * @param $shopId
     *
     * @return Shop
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($shopId)
    {
        $shop = $this->shopFactory->create();
        $this->resource->load($shop, (int)$shopId, 'shop_id');
        if (!$shop->getId()) {
            throw new NoSuchEntityException(
                __('Shop with id "%1" does not exist.', $shopId)
            );
        }
        /** @var RegionInterface $region */
        $region = $this->regionDataFactory->create();
        $region->setRegion($shop->getRegion())
            ->setRegionCode($shop->getRegionCode())
            ->setRegionId($shop->getRegionId());
        $shop->setRegion($region);

        return $shop;
    }

    /**
     * @param $collection
     * @param $lat
     * @param $lon
     * @param bool $useRadius
     * @param null $radiusDistance
     * @return mixed
     */
    protected function addLatLonFilter($collection, $lat, $lon, $useRadius = true, $radiusDistance = null)
    {
        if (!$radiusDistance) {
            $radiusDistance = $this->moduleHelper->getDefaultRadius();
            if (!$useRadius) {
                $radiusDistance = false;
            }
        }

        $units = $this->moduleHelper->getDistanceUnits();
        $collection->addAreaFilter(
            $lat,
            $lon,
            $radiusDistance,
            $units
        );
        return $collection;
    }

    /**
     * @param $filter
     * @param $collection
     * @return \Redbox\Shopfinder\Model\ResourceModel\Shop\Collection
     * @throws LocalizedException
     * @throws \Zend_Json_Exception
     */
    protected function filterByAddress($filter, $collection)
    {
        /** @var  $collection \Redbox\Shopfinder\Model\ResourceModel\Shop\Collection */
        $defaultRadius = $this->moduleHelper->getDefaultRadius();
        $units = $this->moduleHelper->getDistanceUnits();
        $this->geocoder->fetchCoordinates($filter->getValue());
        if ($this->geocoder->getData('error')) {
            throw new LocalizedException(
                __('Unable to geocode address. Please try again.')
            );
        }
        $collection->addAreaFilter(
            $this->geocoder->getData('latitude'),
            $this->geocoder->getData('longitude'),
            $defaultRadius,
            $units
        );
        return $collection;
    }

    /**
     * @param $collection
     * @return array
     */
    protected function buildSearchResultItemsFromCollection($collection)
    {
        $shops = [];
        foreach ($collection as $shopModel) {
            /** @var RegionInterface $region */
            $region = $this->regionDataFactory->create();
            $region->setRegion($shopModel->getRegion())
                ->setRegionCode($shopModel->getRegionCode())
                ->setRegionId($shopModel->getRegionId());
            $shopModel->setRegion($region);
            /** @var \Redbox\Shopfinder\Api\Data\ShopInterface $shopData */
            $shopData = $this->dataShopFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $shopData,
                $shopModel->getData(),
                'Redbox\Shopfinder\Api\Data\ShopInterface'
            );
            $shopData->setImageUrl($shopModel->getImageUrl());
            $shops[] = $shopModel;
        }
        return $shops;
    }

    /**
     * {@inheritDoc}
     */
    public function getAllStoresForBothLanguages()
    {
        $data = [];
        $allShops = $this->shopCollectionFactory->create()->getData();
        $count = count($allShops);
        $i = 0;
        foreach ($allShops as $key => $shop) {
            $i++;
            if ($i == ($count - 1)) {
                break;
            }
            if ($key % 2 !== 0) {
                continue;
            }
            $shop['street_ar'] = $allShops[$key + 1]['street'];
            $shop['city_ar'] = $allShops[$key + 1]['city'];
            $shop['hours_ar'] = $allShops[$key + 1]['other_opentimes_info'];
            $data[] = $shop;
        }
        return $data;
    }
}

define(
    [
        'mage/storage',
        'Beside_Checkout/js/model/resource-url-manager'
    ],
    function (storage, resourceUrlManager) {
        'use strict';

        return {
            getStores: function () {
                return storage.get(
                    resourceUrlManager.getStoresUrl()
                );
            },

            getStoreById: function (id) {
                return storage.get(
                    resourceUrlManager.getStoreByIdUrl(id)
                );
            },
        };
    }
);

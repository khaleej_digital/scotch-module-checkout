define(
    [
        'jquery',
        'ko',
        'underscore',
        'Magento_Checkout/js/view/shipping',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/set-shipping-information',
        'Magento_Checkout/js/checkout-data',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/model/payment/method-converter',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/action/select-shipping-method',
        'Beside_Checkout/js/model/pick-up-form-customer-data',
        'Beside_Checkout/js/action/quote-update',
        'uiRegistry',
        'mage/translate',
        'mage/validation'
    ],
    function (
        $,
        ko,
        _,
        Shipping,
        quote,
        setShippingInformationAction,
        checkoutData,
        customerData,
        stepNavigator,
        fullScreenLoader,
        checkoutDataResolver,
        paymentService,
        methodConverter,
        addressConverter,
        selectBillingAddress,
        selectShippingAddress,
        selectShippingMethodAction,
        pickUpFormCustomerData,
        quoteUpdate,
        registry,
        $t
    ) {
        'use strict';

        return Shipping.extend({
            defaults: {
                shippingMethodListTemplate: 'Beside_Checkout/shipping-address/shipping-method-list'
            },
            shopIdMissing: ko.observable(false),
            shopId: ko.observable(null),
            customerAddress: pickUpFormCustomerData,
            exludeShippingMethods: ko.observableArray(['beside_store_pickup']),
            selectedLocation: ko.observableArray([]),

            initialize: function () {

                let self = this,
                    shippingAddress = checkoutData.getShippingAddressFromData();

                self._super();

                if (shippingAddress !== null && typeof shippingAddress !== 'undefined') {
                    if (typeof shippingAddress.firstname !== 'undefined' && shippingAddress.firstname.length) {
                        self.customerAddress.firstname = shippingAddress.firstname;
                    }
                    if (typeof shippingAddress.lastname !== 'undefined' && shippingAddress.lastname.length) {
                        self.customerAddress.lastname = shippingAddress.lastname;
                    }
                    if (typeof shippingAddress.street !== 'undefined') {
                        if (typeof shippingAddress.street[0] !== 'undefined' && shippingAddress.street[0].length) {
                            self.customerAddress.street = shippingAddress.street[0];
                        }

                        if (typeof shippingAddress.street[1] !== 'undefined' && shippingAddress.street[1].length) {
                            self.customerAddress.streetopt = shippingAddress.street[1];
                        }
                    }
                    if (typeof shippingAddress.telephone !== 'undefined' && shippingAddress.telephone.length) {
                        self.customerAddress.telephone = shippingAddress.telephone;
                    }
                    if (typeof shippingAddress.telephone !== 'undefined' && shippingAddress.telephone.length) {
                        self.customerAddress.email = checkoutData.getInputFieldEmailValue();
                    }
                }
            },

            getFormValue: function(formData, key) {
                let result;

                $.each(formData, function (i, item) {
                    if (item.name === key) {
                        result = item.value;
                        return false;
                    }
                });

                return result;
            },

            getShopData: function(storeIdentifier, key){
                let self = this,
                    result;

                if (self.selectedLocation().length === 0) {
                    let locations = self.customerAddress.locations();
                    $.each(locations, function (i, item) {
                        if (item.location.identifier.toUpperCase() === storeIdentifier.toUpperCase() ) {
                            self.selectedLocation(item.location);
                            return false;
                        }
                    });
                }

                $.each(self.selectedLocation(), function (i, item) {
                    if (i === key) {
                        result = item;
                        return false;
                    }
                });

                return result;
            },

            validateForm: function(form) {
                form.mage('validation', {});
                return form.validation() && form.validation('isValid');
            },

            selectStorePickup: function () {
                let self = this,
                    form = $('#pick-up-location-customer-info'),
                    formData = form.serializeArray(),
                    shop = null;

                fullScreenLoader.startLoader();

                if (!self.validateForm(form)) {
                    fullScreenLoader.stopLoader();
                    return false;
                }

                let billingAddressDetails,
                    shopIdentifier = self.getFormValue(formData, 'pickupLocation'),
                    ispuMethod = {
                        carrier_code: 'beside_store_pickup',
                        method_code: 'beside_store_pickup'
                    },
                    shippingAddressDetails = {
                        firstname: self.getFormValue(formData, 'firstname'),
                        lastname: self.getFormValue(formData, 'lastname'),
                        email: self.getFormValue(formData, 'email'),
                        street: self.getShopData(shopIdentifier, 'street'),
                        city: self.getShopData(shopIdentifier, 'city'),
                        country_id: self.getShopData(shopIdentifier, 'country_id'),
                        telephone: self.getFormValue(formData, 'telephone'),
                        postcode: self.getShopData(shopIdentifier, 'postcode'),
                    };

                shippingAddressDetails = addressConverter.formAddressDataToQuoteAddress(shippingAddressDetails);
                shippingAddressDetails['extension_attributes'] = {
                    pickup_location_id: shopIdentifier,
                    customer_mobile_number_prefix:
                        self.getShopData(shopIdentifier, 'country_id') === 'SA' ? '+966' : '+971'
                };

                billingAddressDetails = shippingAddressDetails;

                self.updateCustomerEmailInOriginalForm(shippingAddressDetails.email);

                self.updateShippingAddress(shippingAddressDetails, billingAddressDetails, ispuMethod);

                $("#payment-step-tab").collapsible("activate");
            },

            updateCustomerEmailInOriginalForm: function (email) {
                let loginFormSelector = 'form[data-role=email-with-possible-login]'

                $(loginFormSelector + ' input[name=username]').val(email);
            },

            updateShippingAddress: function (shippingAddressDetails, billingAddressDetails, ispuMethod) {
                let self = this,
                    quoteId = quote.getQuoteId(),
                    shippingInformationUpdate = new Promise(function (resolve, reject) {
                        resolve(quoteUpdate.shippingInformation(
                            quoteId,
                            shippingAddressDetails,
                            billingAddressDetails,
                            ispuMethod.carrier_code,
                            ispuMethod.method_code
                        ));
                    });

                shippingInformationUpdate.then(function (response) {
                    quote.setTotals(response.totals);
                    paymentService.setPaymentMethods(methodConverter(response['payment_methods']));
                    selectBillingAddress(billingAddressDetails);
                    checkoutDataResolver.resolveBillingAddress();
                    quote.guestEmail = shippingAddressDetails.email;

                    selectShippingMethodAction(ispuMethod);
                    selectShippingAddress(shippingAddressDetails);
                    checkoutData.setShippingAddressFromData(shippingAddressDetails);
                    checkoutData.setValidatedEmailValue(shippingAddressDetails.email);
                    checkoutData.setInputFieldEmailValue(shippingAddressDetails.email);
                    checkoutData.setSelectedShippingRate(ispuMethod.carrier_code + '_' + ispuMethod.method_code);
                    quote.shippingAddress(shippingAddressDetails);

                    stepNavigator.navigateTo('payment');
                    fullScreenLoader.stopLoader();
                }).catch(function (reason) {
                    console.error(reason);
                    fullScreenLoader.stopLoader();
                });
            },

            setShippingInformation: function () {
                if (this.validateShippingInformation()) {
                    fullScreenLoader.startLoader();

                    let self = this,
                        customer = customerData.get('customer')(),
                        addressData,
                        shippingAddressDetails;

                    if (customer.fullname && customer.firstname) {
                        addressData = quote.shippingAddress();

                        addressData['extension_attributes'] = {
                            customer_mobile_number_prefix:
                                addressData.countryId === 'SA' ? '+966' : '+971'
                        };
                    } else {
                        addressData = checkoutData.getShippingAddressFromData();
                    }

                    addressData.email = quote.guestEmail;

                    shippingAddressDetails = addressConverter.formAddressDataToQuoteAddress(addressData);
                    shippingAddressDetails.saveInAddressBook = addressData.saveInAddressBook;
                    shippingAddressDetails['extension_attributes'] = {
                        gift_wrapping: $('[name="gift_wrapping"]:checked').val()
                    };
                    if (shippingAddressDetails) {
                        let phonePrefixForm = $('input[name ="custom_attributes[customer_mobile_number_prefix]"]');

                        if (phonePrefixForm.length) {
                            shippingAddressDetails['extension_attributes'] = {
                                customer_mobile_number_prefix: phonePrefixForm[0].value
                            };
                        }

                        let billingAddressDetails = shippingAddressDetails;

                        self.updateShippingAddress(shippingAddressDetails, billingAddressDetails, quote.shippingMethod());
                    }

                    $('#payment-step-tab').collapsible('activate')
                }
            },
        });
    }
);

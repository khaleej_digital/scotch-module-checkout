<?php

namespace Beside\Checkout\Plugin;

use Magento\Quote\Model\ResourceModel\Quote\Address\Collection;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Framework\App\RequestInterface;

class QuoteAddressCollectionPlugin
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * QuoteAddressCollectionPlugin constructor.
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request
    ) {
        $this->request = $request;
    }

    /**
     * @param Collection $subject
     * @param Collection $result
     * @return mixed
     */
    public function afterLoadWithFilter(Collection $subject, Collection $result)
    {
        foreach ($result as $item) {
            $this->setPickupLocationIdExtensionAttribute($item);
        }

        return $result;
    }

    /**
     * @param AddressInterface $address
     * @return AddressInterface
     */
    private function setPickupLocationIdExtensionAttribute(AddressInterface &$address)
    {
        if (strpos($this->request->getRequestString(), 'shipping-information')) {
            return $address;
        }

        $extensionAttributes = $address->getExtensionAttributes();
        $pickupLocationId = $address->getPickupLocationId();

        if ($pickupLocationId) {
            $extensionAttributes->setPickupLocationId($pickupLocationId);
            $address->setExtensionAttributes($extensionAttributes);
        }

        return $address;
    }
}

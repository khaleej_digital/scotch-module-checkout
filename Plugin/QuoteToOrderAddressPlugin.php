<?php

namespace Beside\Checkout\Plugin;

class QuoteToOrderAddressPlugin
{
    /**
     * @param \Magento\Quote\Model\Quote\Address\ToOrderAddress $subject
     * @param \Magento\Sales\Api\Data\OrderAddressInterface $result
     * @param \Magento\Quote\Api\Data\AddressInterface $quoteAddress
     * @param array $data
     * @return \Magento\Sales\Api\Data\OrderAddressInterface
     */
    public function afterConvert(
        \Magento\Quote\Model\Quote\Address\ToOrderAddress $subject,
        \Magento\Sales\Api\Data\OrderAddressInterface $result,
        \Magento\Quote\Api\Data\AddressInterface $quoteAddress,
        $data = []
    ) {
        $pickupLocationId = $quoteAddress->getExtensionAttributes()->getPickupLocationId();

        if (empty($pickupLocationId)) {
            $pickupLocationId = $quoteAddress->getPickupLocationId();
        }

        $result->setPickupLocationId($pickupLocationId);

        return $result;
    }
}

<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Checkout\Plugin\Model\Lpm;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Config
 * @package Beside\Checkout\Plugin\Model\Lpm
 */
class Config
{
    const KEY_ALLOWED_METHODS = 'allowed_methods';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param \PayPal\Braintree\Model\Lpm\Config $subject
     * @param callable $proceed
     * @return array
     */
    public function aroundGetAllowedMethods(
        \PayPal\Braintree\Model\Lpm\Config $subject,
        callable $proceed
    ) {
        if (!$this->scopeConfig->getValue(self::KEY_ALLOWED_METHODS)) {
            return [];
        }

        return $proceed();
    }
}

<?php

namespace Beside\Checkout\Plugin;

use Magento\Quote\Model\ResourceModel\Quote\Address;
use Magento\Quote\Api\Data\AddressInterface;

class SaveAddress
{
    /**
     * @param $subject
     * @param $address
     */
    public function beforeSave(Address $subject, AddressInterface $address)
    {
        if ($address->getExtensionAttributes() && !empty($address->getExtensionAttributes()->getCustomerMobileNumberPrefix())) {
            $address->setCustomerMobileNumberPrefix($address->getExtensionAttributes()->getCustomerMobileNumberPrefix());
        }

        if ($address->getExtensionAttributes() && !empty($address->getExtensionAttributes()->getPickupLocationId())) {
            $address->setPickupLocationId($address->getExtensionAttributes()->getPickupLocationId());
        }
    }
}

<?php
/**
 * @category  Beside
 * @package   Beside_Checkout
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Checkout\Plugin;

use Psr\Log\LoggerInterface;
use Brick\PhoneNumber\PhoneNumber;
use Brick\PhoneNumber\PhoneNumberFormat;
use Brick\PhoneNumber\PhoneNumberParseException;

/**
 * Class SaveAddressInformation
 * @package Beside\Checkout\Plugin
 */
class SaveAddressInformation
{
    /**
     * @var Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * SaveAddressInformation constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ) {
        $this->logger = $logger->withName(__CLASS__);
        $this->quoteRepository = $quoteRepository;
    }
    /**
     * @param \Magento\Checkout\Api\ShippingInformationManagementInterface $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Api\ShippingInformationManagementInterface $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $shippingAddress = $addressInformation->getShippingAddress();
        $billingAddress = $addressInformation->getBillingAddress();

        $giftWrapping = null;
        if ($shippingAddress->getExtensionAttributes()
            && !empty($shippingAddress->getExtensionAttributes()->getGiftWrapping())
        ) {
            $giftWrapping = $shippingAddress->getExtensionAttributes()->getGiftWrapping();
        }
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->setGwId($giftWrapping);
        $this->quoteRepository->save($quote);

        try {
            $shippingTelephone = PhoneNumber::parse($shippingAddress->getTelephone());
            $billingTelephone = PhoneNumber::parse($billingAddress->getTelephone());
            $shippingAddress->setTelephone($shippingTelephone->getNationalNumber());
            $billingAddress->setTelephone($billingTelephone->getNationalNumber());
            $shippingAddress->getExtensionAttributes()
                ->setCustomerMobileNumberPrefix('+' . $shippingTelephone->getCountryCode());
            $billingAddress->getExtensionAttributes()
                ->setCustomerMobileNumberPrefix('+' . $billingTelephone->getCountryCode());
        } catch (PhoneNumberParseException $exception) {
            $this->logger->error(sprintf('There has been an error while parsing phone number %s', $shippingAddress->getTelephone()), ['exception' => $exception]);
        }
    }
}

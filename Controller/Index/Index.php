<?php
/**
 * @category  Beside
 * @package   Beside_Checkout
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */


namespace Beside\Checkout\Controller\Index;

/**
 * Override for checkout url
 *
 * Class Index
 * @package Beside\Checkout\Controller\Index
 */
class Index extends \Magento\Checkout\Controller\Index\Index
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if (!$this->canCheckout()) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setUrl($this->_url->getUrl('checkout/login'));
            $this->_customerSession->setBeforeAuthUrl($this->_url->getUrl('checkout'));

            return $resultRedirect;
        }

        return parent::execute();
    }

    /**
     * @return bool
     */
    private function canCheckout()
    {
        return $this->getOnepage()->getCustomerSession()->isLoggedIn()
            || $this->getOnepage()->getQuote()->getCustomerEmail();
    }
}
